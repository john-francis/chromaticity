<p>
  <img src="https://api.travis-ci.org/johnfrancisgit/chromaticity.svg?branch=master">
</p>

<p align="center"> 
<img src="img/screenshots/product_screenshot.png">
</p>


## About 
Desktop application to control Philips hue lights.
Linux, Mac and Windows will be supported.

Built with electron will allow the application to be cross platform.
Currently the application is still in the very early stages of development and no versions (stable, beta or alpha) have as of yet been released.

## Installation
* `npm install`
* `npm run start`
